package com.atlassian.jira.plugins.mailqueueapi.rest;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.mail.queue.MailQueueItem;

import java.util.Queue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("stats")
public class MailQueueResource {

	private final JiraAuthenticationContext authenticationContext;
	private final GlobalPermissionManager globalPermissionManager;
	private final MailQueue mailQueue;

	public MailQueueResource(final JiraAuthenticationContext authenticationContext,
			                 final GlobalPermissionManager globalPermissionManager,
			                 final MailQueue mailQueue) {
		this.authenticationContext = authenticationContext;
		this.globalPermissionManager = globalPermissionManager;
		this.mailQueue = mailQueue;
	}

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getQueueSize() {
		ApplicationUser user = authenticationContext.getUser();

		if (user == null) {
			return Response.status(401).build();
		}

		if (!globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)) {
			return Response.status(403).build();
		}

		Queue<MailQueueItem> mainQueue = mailQueue.getQueue();
		Queue<MailQueueItem> errorQueue = mailQueue.getErrorQueue();

		CacheControl cacheControl = new CacheControl();
		cacheControl.setNoCache(true);
		cacheControl.setNoStore(true);

		return Response.ok(new StatsBean(mainQueue.size(), errorQueue.size())).cacheControl(cacheControl).build();
	}
}
